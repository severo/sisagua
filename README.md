# sisagua

The goal of sisagua is to produce statistics and reports on the SISAGUA data related to drinking water quality in Brazil.

## Installation

[TODO]

You can install the released version of sisagua from [CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("sisagua")
```

## Example

[TODO]

This is a basic example which shows you how to solve a common problem:

``` r
## basic example code
```

## Development

We use the [tidyverse style guide]( https://style.tidyverse.org/).

To make it easier to manage the package, see [Writing an R package from scratch](https://r-mageddon.netlify.com/post/writing-an-r-package-from-scratch/). Load the libraries:

```R
library(devtools)
library(roxygen2)
library(usethis)
```
