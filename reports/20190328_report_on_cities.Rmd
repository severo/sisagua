---
title: "Pesticides in Brazilian drinking water - Report on cities"
author: "Sylvain LESAGE"
date: "28 march 2019"
---

[//]: # (This is a comment)
[//]: # (Change "pdf_document" to "word_document" to export to a .docx file)
[//]: # (Try to run the followinf macro to get the tables to a good looking format: https://ask.libreoffice.org/en/question/18664/optimal-column-width-for-all-tables-in-odt-documents/)

```{r setup, include=FALSE}
library(dplyr)
library(sisagua)
population <- sisagua::get_population_data()

controle_data <- get_controle_data()

pesticides_limits <- controle_data %>%
  distinct(parameter, maximum_allowed_value_brazil, maximum_allowed_value_europe) %>%
  arrange(parameter)

# A. List of cities that never did any test, with information of population and state

# List of cities that tested at least once
muns_tested = controle_data %>%
  group_by(ibge_code) %>%
  summarize(testsCount = n())
# Filter population data: only keep the cities that are not in the listOfTestedMun
pop_never_tested = population %>%
  anti_join(muns_tested, by = "ibge_code")
# Export
sisagua::export_data_to_xlsx(pop_never_tested, "/tmp/", "never_tested_2014_to_2017.xlsx")

# B. List of cities that detected 27 pesticides in four years

# List of cities that detected the 27
muns_27_detected_in_4_years = controle_data %>%
  filter(is_detected) %>%
  group_by(ibge_code) %>%
  summarize(num_parameters = n_distinct(parameter)) %>%
  filter(num_parameters == 27)
# Filter population data
pop_27_detected_in_4_years = population %>%
  semi_join(muns_27_detected_in_4_years, by = "ibge_code")
# Export
sisagua::export_data_to_xlsx(pop_27_detected_in_4_years, "/tmp/", "27_detected_2014_to_2017.xlsx")
# With one line per substance above Brazilian limit and per municipality
muns_27_detected_in_4_years_subst_above_br = controle_data %>%
  inner_join(pop_27_detected_in_4_years, by = "ibge_code") %>%
  filter(`min_x>br` == TRUE) %>%
  group_by(ibge_code, municipality_name, estimated_population, state_letters_code.x, state_name, parameter) %>%
  summarize(num_tests_above_brazil_limit = n())
# Export
sisagua::export_data_to_xlsx(muns_27_detected_in_4_years_subst_above_br, "/tmp/", "27_detected_2014_to_2017_with_substances_above_brazil_limit.xlsx")
# With one line per substance above Brazilian limit and per municipality
muns_27_detected_in_4_years_subst_above_eu = controle_data %>%
  inner_join(pop_27_detected_in_4_years, by = "ibge_code") %>%
  filter(`min_x>eu` == TRUE) %>%
  group_by(ibge_code, municipality_name, estimated_population, state_letters_code.x, state_name, parameter) %>%
  summarize(num_tests_above_brazil_limit = n())
# Export
sisagua::export_data_to_xlsx(muns_27_detected_in_4_years_subst_above_eu, "/tmp/", "27_detected_2014_to_2017_with_substances_above_europe_limit.xlsx")

# C. List of cities that detected 27 pesticides over the same year

# List of cities that detected the 27 over the same year
muns_27_detected_in_1_year = controle_data %>%
    filter(is_detected)%>%
  group_by(ibge_code, year) %>%
  summarize(num_parameters = n_distinct(parameter)) %>%
  filter(num_parameters == 27) %>%
  group_by(ibge_code) %>%
  summarize(num_years = n_distinct(year))
# Filter population data
pop_27_detected_in_1_year = population %>%
  semi_join(muns_27_detected_in_1_year, by = "ibge_code")
# Export
sisagua::export_data_to_xlsx(pop_27_detected_in_1_year, "/tmp/", "27_detected_1_year.xlsx")
# With one line per substance above Brazilian limit and per municipality
muns_27_detected_in_1_year_subst_above_br_by_year = controle_data %>%
    filter(is_detected)%>%
  group_by(ibge_code, year) %>%
  summarize(num_parameters = n_distinct(parameter)) %>%
  filter(num_parameters == 27) %>%
  inner_join(controle_data, by = c("ibge_code","year")) %>%
  inner_join(population, by = "ibge_code") %>%
  filter(`min_x>br` == TRUE) %>%
  group_by(ibge_code, municipality_name, estimated_population, state_letters_code.x, state_name, parameter, year) %>%
  summarize(num_tests_above_brazil_limit = n())
# Export
sisagua::export_data_to_xlsx(muns_27_detected_in_1_year_subst_above_br_by_year, "/tmp/", "27_detected_1_year_with_substances_above_brazil_limit_in_that_year.xlsx")
# With one line per substance above Brazilian limit and per municipality
muns_27_detected_in_1_year_subst_above_eu_by_year = controle_data %>%
    filter(is_detected)%>%
  group_by(ibge_code, year) %>%
  summarize(num_parameters = n_distinct(parameter)) %>%
  filter(num_parameters == 27) %>%
  inner_join(controle_data, by = c("ibge_code","year")) %>%
  inner_join(population, by = "ibge_code") %>%
  filter(`min_x>eu` == TRUE) %>%
  group_by(ibge_code, municipality_name, estimated_population, state_letters_code.x, state_name, parameter, year) %>%
  summarize(num_tests_above_europe_limit = n())
# Export
sisagua::export_data_to_xlsx(muns_27_detected_in_1_year_subst_above_eu_by_year, "/tmp/", "27_detected_1_year_with_substances_above_europe_limit_in_that_year.xlsx")

```
