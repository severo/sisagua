---
title: "Sorriso"
author: "Sylvain LESAGE"
date: "20 february 2019"
output:
  pdf_document: default
---

[//]: # (This is a comment)
[//]: # (Change "pdf_document" to "word_document" to export to a .docx file)
[//]: # (Try to run the followinf macro to get the tables to a good looking format: https://ask.libreoffice.org/en/question/18664/optimal-column-width-for-all-tables-in-odt-documents/)

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::knit_hooks$set(inline = function(x) {
  prettyNum(x, big.mark=",")
})
library(dplyr)
```

```{r echo=FALSE}
library(sisagua)
population <- sisagua::get_population_data()
ibge_code_report = "510792"
mun <- population %>% filter(ibge_code == ibge_code_report)
name <- mun$municipality_name
```

# Pesticides in `r name`

This is a report on the pesticide concentration in drinking water of the `r name` municipality, based on the analysis of SISAGUA data.

## Controls

```{r echo=FALSE, include=FALSE}
controle_data <- get_controle_data() %>%
  filter(ibge_code == ibge_code_report)

pesticides_limits <- controle_data %>%
  distinct(parameter, maximum_allowed_value_brazil, maximum_allowed_value_europe) %>%
  arrange(parameter)
```

- `r nrow(controle_data)` controls in `r name`
- by date of measurement:

```{r echo=FALSE}
by_date <- controle_data %>%
  group_by(collect_date) %>%
  summarize(num_controls = n()) %>%
  arrange(as.Date(collect_date, "%d/%m/%Y"))

knitr::kable(
  by_date,
  caption = paste('Number of tests by collect date in', name),
  row.names = FALSE,
  col.names = c("Date", "Number of measurements"),
  format.args = list(big.mark = ',')
)
```

- by form title:

```{r echo=FALSE}
by_form_title <- controle_data %>%
  group_by(form_title) %>%
  summarize(num_controls = n())

knitr::kable(
  by_form_title,
  caption = paste('Number of tests by form title in', name),
  row.names = FALSE,
  col.names = c("Form title", "Number of measurements"),
  format.args = list(big.mark = ',')
)
```


- by plant:

```{r echo=FALSE}
by_plant <- controle_data %>%
  group_by(water_purification_plant) %>%
  summarize(num_controls = n())

knitr::kable(
  by_plant,
  caption = paste('Number of tests by plant in', name),
  row.names = FALSE,
  col.names = c("Plant", "Number of measurements"),
  format.args = list(big.mark = ',')
)
```

- by measurement point:

```{r echo=FALSE}
by_point <- controle_data %>%
  group_by(measurement_point) %>%
  summarize(num_controls = n())

knitr::kable(
  by_point,
  caption = paste('Number of tests by measurement point in', name),
  row.names = FALSE,
  col.names = c("Measurement point", "Number of measurements"),
  format.args = list(big.mark = ',')
)
```

- by pesticide:


```{r echo=FALSE}
by_pesticide <- controle_data %>%
  group_by(parameter) %>%
  summarize(
    num_controls = n(),
    detected = length(parameter[is_detected == TRUE]),
    supeu = length(parameter[`min_x=eu` | `min_x>eu`]),
    supbr = length(parameter[`min_x=br` | `min_x>br`])
  )

knitr::kable(
  by_pesticide,
  caption = paste('Number of tests by type of pesticide in', name),
  row.names = FALSE,
  col.names = c("Pesticide", "Measurements", "Detected", ">= EU", ">= BR"),
  format.args = list(big.mark = ',')
)
```
