---
title: "Profenofós"
author: "Sylvain LESAGE"
date: "14 novembre 2019"
output:
  pdf_document: default
  word_document: default
---

```{r setup, include=FALSE}
library(knitr)
knitr::opts_chunk$set(echo = TRUE)
knitr::knit_hooks$set(inline = function(x) {
  prettyNum(x, big.mark=",")
})
library(dplyr)
```

```{r echo=FALSE}
library(sisagua)
population <- sisagua::get_population_data()
```

# Profenofós in Brazil

This is a report on the detections of profenofós in Brazil (SISAGUA data).

```{r echo=FALSE}
pesticide <- "Profenofós"
states <- merge(get_num_municipalities_by_state(), get_estimated_population_by_state())
states <- states[c(
  "state_letters_code",
  "state_name",
  "state_numbers_code",
  "num_municipalities",
  "pct_num_municipalities",
  "estimated_population",
  "pct_estimated_population"
  )]
```

```{r echo=FALSE, include=FALSE}
controle_data <- get_controle_data()

pesticides_limits <- controle_data %>%
  distinct(parameter, maximum_allowed_value_brazil, maximum_allowed_value_europe) %>%
  arrange(parameter)
```

The Brazilian legal limit for the concentration of profenofós is `r pesticides_limits %>% filter(parameter == pesticide) %>% pull(maximum_allowed_value_brazil)` $\mu g/L$, whereas the European limit is `r pesticides_limits %>% filter(parameter == pesticide) %>% pull(maximum_allowed_value_europe)` $\mu g/L$.

Important note: in this report, we exclude the "<LQ" measurements from detections.
```{r echo=FALSE}
controle_pesticide <- controle_data %>%
  filter(parameter == pesticide)
stats <- controle_pesticide %>%
  summarize(
    measurements = n(),
    detected_abs = sum(is_detected & !is_below_quantitation_limit),
    detected     = sisagua::pct(detected_abs, measurements),
    sup_eu_abs   = sum(`min_x>eu` | `min_x=eu`),
    sup_eu       = sisagua::pct(sup_eu_abs, measurements),
    eq_br_abs    = sum(`min_x=br`),
    eq_br        = sisagua::pct(eq_br_abs, measurements),
    sup_br_abs   = sum(`min_x>br`),
    sup_br       = sisagua::pct(sup_br_abs, measurements)
  )

stats <- stats[c(
  "measurements",
  "detected",
  "sup_eu",
  "eq_br",
  "sup_br"
)]

options(knitr.kable.NA = '-')
knitr::kable(
  stats,
  caption = 'Measurements of Profenofós by state (%: ref to number of measurements)',
  row.names = TRUE,
  col.names = c("Measures", "% detected", "% >= EU", "% = BR", "% > BR"),
  format.args = list(big.mark = ',')
)
```

```{r echo=FALSE}
by_year <- controle_data %>%
  filter(parameter == pesticide) %>%
  group_by(year) %>%
  summarize(
    measurements = n(),
    detected_abs = sum(is_detected & !is_below_quantitation_limit),
    detected     = sisagua::pct(detected_abs, measurements),
    sup_eu_abs   = sum(`min_x>eu` | `min_x=eu`),
    sup_eu       = sisagua::pct(sup_eu_abs, measurements),
    eq_br_abs    = sum(`min_x=br`),
    eq_br        = sisagua::pct(eq_br_abs, measurements),
    sup_br_abs   = sum(`min_x>br`),
    sup_br       = sisagua::pct(sup_br_abs, measurements)
  )

by_year<- by_year[c(
  "year",
  "measurements",
  "detected",
  "sup_eu",
  "eq_br",
  "sup_br"
)]

options(knitr.kable.NA = '-')
knitr::kable(
  by_year,
  caption = 'Measurements of Profenofós by state (%: ref to number of measurements)',
  row.names = TRUE,
  col.names = c("Year", "Measures", "% detected", "% >= EU", "% = BR", "% > BR"),
  format.args = list(big.mark = ',')
)
```
